var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var JTBooking = require('../models/JTBooking.js');
var JTGuest = require('../models/JTGuest.js');
var JTRoom = require('../models/JTRoom.js');

router.get('/', function (req, res, next) {
  "use strict";
  JTBooking.find({}).populate('rooms guests').exec(function (err, bookings) {
    if (err) { return next(err); }
    res.json(bookings);
  });
});

router.get('/info/:id', function (req, res, next) {
  "use strict";
  JTBooking.findById(req.params.id).populate('rooms guests').exec(function (err, bookings) {
    if (err) { return next(err); }
    res.json(bookings);
  });
});

router.get('/id/:id', function (req, res, next) {
  "use strict";
  JTBooking.findById(req.params.id).exec(function (err, booking) {
    if (err) { return next(err); }
    res.json(booking);
  });
});

router.put('/id/:id', function (req, res, next) {
  "use strict";
  if (req.body.hasOwnProperty('_id')) {
    delete req.body['_id'];
  }
  JTBooking.findOneAndUpdate({ _id: req.params.id }, req.body, function (err, booking) {
    if (err) { return next(err); }
    res.json(booking);
  });
});

router.delete('/id/:id', function (req, res, next) {
  "use strict";
  JTBooking.findById(req.params.id, function (err, booking) {
    if (err) { return next(err); }
    if (booking === null) { return next(); }
    var roomIds = booking.rooms.map(function (el, i, arr) {return mongoose.Types.ObjectId(el); });
    JTRoom.update({_id: {$in: roomIds}}, {available: 'true'}, {multi: true}).exec();
    JTGuest.find().where('_id').in(booking.guests).remove().exec();
  });

  JTBooking.findByIdAndRemove(req.params.id).exec(function (err) {
    if (err) { return next(err); }
    res.json({ status: "ok"});
  });
});

router.post('/new', function (req, res, next) {
  "use strict";
  if (req.body.hasOwnProperty('_id')) {
    delete req.body['_id'];
  }
  JTBooking.create(req.body, function (err, booking) {
    if (err) { return next(err); }
    var roomIds = booking.rooms.map(function (el, i, arr) {return mongoose.Types.ObjectId(el); });
    JTRoom.update({_id: {$in: roomIds}}, {available: 'false'}, {multi: true}).exec();
    JTBooking.findById(booking.id).populate('rooms guests').exec(function (err, bookings) {
      if (err) { return next(err); }
      res.json(bookings);
    });
  });
});
module.exports = router;
