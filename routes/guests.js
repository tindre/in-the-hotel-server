var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var JTGuest = require('../models/JTGuest.js');

router.get('/', function (req, res, next) {
  "use strict";
  JTGuest.find(req.query).exec(function (err, guests) {
    if (err) { return next(err); }
    res.json(guests);
  });
});

router.get('/id/:id', function (req, res, next) {
  "use strict";
  JTGuest.findById(req.params.id).exec(function (err, guest) {
    if (err) { return next(err); }
    res.json(guest);
  });
});

router.put('/id/:id', function (req, res, next) {
  "use strict";
  if (req.body.hasOwnProperty('_id')) {
    delete req.body['_id'];
  }
  JTGuest.findOneAndUpdate({ _id: req.params.id }, req.body, function (err, guest) {
    if (err) { return next(err); }
    res.json(guest);
  });
});

router.delete('/id/:id', function (req, res, next) {
  "use strict";
  JTGuest.findByIdAndRemove(req.params.id, function (err, guest) {
    if (err) { return next(err); }
    res.json({ status: "ok"});
  });
});

router.post('/new', function (req, res, next) {
  "use strict";
  console.log(req.body);
  if (req.body.hasOwnProperty('_id')) {
    delete req.body['_id'];
  }
  JTGuest.create(req.body, function (err, guest) {
    if (err) { return next(err); }
    res.json(guest);
  });
});

module.exports = router;
