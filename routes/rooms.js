var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var noCache = require('connect-nocache')();

var JTRoom = require('../models/JTRoom.js');

router.get('/', noCache, function (req, res, next) { "use strict"; next(); });

router.get('/', function (req, res, next) {
  "use strict";
  console.log(req.get('Content-Type'));
  JTRoom.find(req.query).exec(function (err, rooms) {
    if (err) { return next(err); }
    res.json(rooms);
  });
});

router.get('/id/:id', function (req, res, next) {
  "use strict";
  JTRoom.findById(req.params.id).exec(function (err, room) {
    if (err) { return next(err); }
    res.json(room);
  });
});

router.put('/id/:id', function (req, res, next) {
  "use strict";
  if (req.body.hasOwnProperty('_id')) {
    delete req.body['_id'];
  }
  JTRoom.findOneAndUpdate({ _id: req.params.id }, req.body, function (err, room) {
    if (err) { return next(err); }
    res.json(room);
  });
});

router.get('/one', function (req, res, next) {
  "use strict";
  JTRoom.findOne(req.query).exec(function (err, room) {
    if (err) { return next(err); }
    res.json(room);
  });
});

router.get('/where/:field', function (req, res, next) {
  "use strict";
  var queryStr = '{ "' + req.params.field + '": {';
  for (var cmd in req.query) {
    queryStr += '"$' + cmd + '": ' + req.query[cmd] + ',';
  }
  queryStr = queryStr.substring(0, queryStr.length - 1);
  queryStr += '}}';
  console.log(queryStr);
  JTRoom.find(JSON.parse(queryStr), function (err, rooms) {
    console.log(rooms);
    if (err) { return next(err); }
    res.json(rooms);
  });
});

router.post('/query/', function (req, res, next) {
  "use strict";
  JTRoom.find(req.body, function (err, rooms) {
    if (err) { return next(err); }
    res.json(rooms);
  });
});

module.exports = router;
