var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*
  name: { type: String, required: true },
  surname: { type: String, required: true },
*/

var JTGuestSchema = new Schema({

  adult: { type: String, required: true },
  note: String
}, { collection: 'guests'});

module.exports = mongoose.model('JTGuest', JTGuestSchema);
