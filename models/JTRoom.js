var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var JTRoomSchema = new Schema({
  number: Number,
  beds: Number,
  price: Number,
  discount: Number,
  available: String,
  note: String
}, { collection: 'rooms'});

module.exports = mongoose.model('JTRoom', JTRoomSchema);

