var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var guest = require('./JTGuest.js');
var room = require('./JTRoom.js');

var JTBookingSchema = new Schema({
  guests: [{ type: Schema.Types.ObjectId, ref: 'JTGuest' }],
  rooms: [{ type: Schema.Types.ObjectId, ref: 'JTRoom' }],
  from: Date,
  to: Date,
  note: String
}, { collection: 'bookings'});

module.exports = mongoose.model('JTBooking', JTBookingSchema);
