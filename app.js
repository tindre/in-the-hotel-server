/**
 * Module dependencies.
 */
var debug = require('debug')('ItH-Server');
var http = require('http');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var bookings = require('./routes/bookings');
var rooms = require('./routes/rooms');
var guests = require('./routes/guests');

var app = express();

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  "use strict";
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Get port from environment and store in Express.
 */
var ip_addr = process.env.OPENSHIFT_NODEJS_IP   || '127.0.0.1';
var port    = normalizePort(process.env.OPENSHIFT_NODEJS_PORT) || '8080';

var tunel_db_user = "admin";
var tunel_db_password = "X36wBwRMDRQ7";

//DB setup
// default to a 'localhost' configuration:
var connection_string = tunel_db_user + ":" + tunel_db_password + '@127.0.0.1:27017/nodejs';
// if OPENSHIFT env variables are present, use the available connection info:
if (process.env.OPENSHIFT_MONGODB_DB_PASSWORD) {
  connection_string = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
    process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
    process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
    process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
    process.env.OPENSHIFT_APP_NAME;
}
console.log(connection_string);
mongoose.set('debug', true);
mongoose.connect('mongodb://' + connection_string, function (err) {
  "use strict";
  if (err) {
    console.log('connection error', err);
  } else {
    console.log('connection successful');
  }
});

// Mongoose models set up

// middleware config
app.disable('tag');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'text/plain' }));
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/bookings', bookings);
app.use('/rooms', rooms);
app.use('/guests', guests);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  "use strict";
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  "use strict";
  res.status(err.status || 500).json({ "error": err.message });
});

app.set('port', port);

var server = app.listen(port, ip_addr, function () {
  "use strict";
  console.log('%s: Node server started on %s:%d ...', Date(Date.now()), ip_addr, port);
});

